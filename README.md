# URL shortener

В данном репозитории представлена базовая реализация сервиса по предоставлению коротких ссылок.

## Описание задачи

Сервис обрабатывает запрос пользователя с длинной ссылкой. В ответе
сервис возвращает короткую ссылку. При переходе по короткой ссылке в
браузере должна открываться страница по длинной ссылке.

### Принцип работы

Пользователь делает POST-запрос по API к сервису, в запросе передаёт ссылку
для сокращения. На стороне сервиса применяется логика сокращения
ссылки: входные данные валидируются, генерируется id короткой ссылки.
Эти данные записываются в БД, затем пользователю возвращается ответ в
JSON-формате вида:
```json
{
  "short_url": "<адрес сервера>/<сгенерированный id>"
}
```

например:
```json
{
  "short_url": "http://localhost:8080/short/Ugn3l1"
}
```

При переходе по короткой ссылке сервис должен перенаправить
пользователя на страницу, на которую указывала исходная длинная ссылка.
Например, при обращении по ссылке `http://localhost:8080/short/Ugn3l1`
сервис должен найти в БД id короткой ссылки «Ugn3l1», соответствующую
длинную ссылку и перенаправить пользователя на страницу, на которую
указывала исходная длинная ссылка (HTTP-код 301).

### Что требуется реализовать

- REST API для выполнения CRUD-операций со ссылками
- эндпоинт для приёма запросов по коротким ссылкам с последующим редиректом
- структуру базы данных для хранения связей между длинными и короткими ссылками
- логику, реализующую алгоритм сокращения ссылки до 6 символов

## Документация OpenAPI (Swagger)

Файл с документацией: `./src/app/api/openapi.json`

Для создания/обновления документации:
```sh
poetry run python generate_doc.py
```

## Локальное развертывание

```sh
poetry install
docker run -d -e POSTGRES_PASSWORD='admin' -e POSTGRES_USER='admin' -e POSTGRES_DB='url_shortener' -p 5432:5432 --name=postgres-test postgres
poetry run alembic upgrade head
poetry run python src/app/service.py
```

## Локальная сборка образа

```sh
docker build .
```

## Локальный запуск тестов

```sh
poetry install --with test
docker run -d -e POSTGRES_PASSWORD='admin' -e POSTGRES_USER='admin' -e POSTGRES_DB='url_shortener' -p 5432:5432 --name=postgres-test postgres
poetry run pytest --cov=src tests
```

## Локальный запуск линтера

```sh
poetry install --with lint
poetry run flake8 src
```

##  Развертывание и остановка сервиса в Kubernetes

```sh
helm install <release_name> helm
kubectl exec <pod_identifier> -- alembic upgrade head
helm uninstall <release_name>
```