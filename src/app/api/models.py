"""Модуль содержит pydantic модели для валидации данных."""

from pydantic import BaseModel, Field


class UrlRequest(BaseModel):
    """Запрос на сокращение ссылки."""

    url: str = Field(max_length=1000)


class ShortUrlResponse(BaseModel):
    """Ответ на запрос сокращения ссылки."""

    short_url: str
