"""Модуль содержит эндпоинты отображающие текущее состояние сервиса."""

from typing import Annotated

from fastapi import APIRouter, Depends, status
from fastapi.responses import Response
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import text

from src.app.data_sources.adaptor import get_session

router = APIRouter()


@router.get('/healthz/up')
async def up():
    """Liveness проба, сервис запустился.

    Returns:
        Response: статус сервиса
    """
    return Response(
        status_code=status.HTTP_200_OK,
    )


@router.get('/healthz/ready')
async def ready(
    session: Annotated[AsyncSession, Depends(get_session)],
):
    """Ready проба, сервис готов принимать запросы.

    Args:
        session (AsyncSession): сессия подключения к бд

    Returns:
        Response: статус сервиса
    """
    try:
        await session.execute(text('SELECT 1'))
    except ConnectionRefusedError:
        return Response(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)
    return Response(status_code=status.HTTP_200_OK)
