"""Модуль является точкой входа в приложение."""

import uvicorn
from fastapi import FastAPI

from src.app import healthz
from src.app.config.config import settings
from src.app.urls import controller as urls_controller

app = FastAPI()
app.include_router(healthz.router)
app.include_router(urls_controller.router)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=settings.service.host,
        port=settings.service.port,
    )
