"""Модуль содержит эндпоинты связанные с бизнес логикой получения коротких и полных ссылок."""

from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import RedirectResponse
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.api.models import ShortUrlResponse, UrlRequest
from src.app.config.config import settings
from src.app.data_sources.adaptor import get_session
from src.app.data_sources.url_storage import UrlStorage

router = APIRouter()
url_storage = UrlStorage()


@router.post('/short')
async def get_short_url(
    url_request: UrlRequest,
    session: Annotated[AsyncSession, Depends(get_session)],
):
    """Метод сохраняет url и возвращает короткий url.

    Args:
        url_request (UrlRequest): url ссылка
        session (AsyncSession): сессия подключения к бд

    Returns:
        ShortUrlResponse: короткая url ссылка
    """
    url_id = await url_storage.add_url(session=session, url=url_request.url)
    return ShortUrlResponse(
        short_url='http://localhost:{0}/short/{1}'.format(
            settings.service.port,
            url_id,
        ),
    )


@router.get('/short/{url_id}')
async def get_full_url(
    url_id: str,
    session: Annotated[AsyncSession, Depends(get_session)],
):
    """Метод для получения полной ссылки.

    Args:
        url_id (str): id полной ссылки
        session (AsyncSession): сессия подключения к бд

    Raises:
        HTTPException: cсылки с таким id не существует

    Returns:
        RedirectResponse: редирект на полную ссылку
    """
    full_url = await url_storage.get_url(session=session, url_id=url_id)
    if not full_url:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Ссылки с таким id не существует',
        )
    return RedirectResponse(
        url=full_url,
        status_code=status.HTTP_301_MOVED_PERMANENTLY,
    )
