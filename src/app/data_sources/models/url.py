"""Модуль содержит orm модель url."""
from sqlalchemy import Column, String

from src.app.config.config import settings
from src.app.data_sources.models.base import Base


class UrlAlchemyModel(Base):
    """Класс описывает orm модель url.

    Args:
        Base (DeclarativeMeta): базовая orm модель
    """

    __tablename__ = 'urls'

    id = Column(String(length=settings.urls.id_length), primary_key=True)
    url = Column(String(length=1000), nullable=False)  # noqa: WPS432
