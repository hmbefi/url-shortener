"""Модуль содержит класс UrlSotrage."""

from sqlalchemy import insert, select
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.config.config import settings
from src.app.data_sources.id_generator import generate
from src.app.data_sources.models import UrlAlchemyModel


class UrlStorage(object):
    """Хранилище url адресов."""

    url_storage: dict = {}

    async def get_url(self, session: AsyncSession, url_id: str) -> str | None:
        """Получить полный url адрес.

        Args:
            session: (AsyncSession): сессия подключения к бд
            url_id (str): id адреса

        Returns:
            str | None: полный url адрес
        """
        selected_row = (await session.execute(
            select(UrlAlchemyModel).where(
                UrlAlchemyModel.id == url_id,
            ),
        )).scalar()
        if selected_row:
            return selected_row.url

    async def add_url(self, session: AsyncSession, url: str) -> str | None:
        """Добавить новый url адрес.

        Args:
            session: (AsyncSession): сессия подключения к бд
            url (str): url адрес

        Returns:
            str | None: короткий id или None если создание короткой ссылки не удалось
        """
        for _ in range(settings.urls.generation_retries):
            url_id = generate()
            existed_id = await self.get_url(session=session, url_id=url_id)
            if not existed_id:
                await session.execute(
                    insert(UrlAlchemyModel).values(
                        id=url_id,
                        url=url,
                    ),
                )
                await session.commit()
                return url_id
