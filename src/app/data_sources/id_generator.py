"""Модуль содержит метод для генерации короткого id."""

import string
from random import choices

from src.app.config.config import settings


def generate() -> str:
    """Сгенерировать короткий id.

    Returns:
        str: короткий id
    """
    return ''.join(choices(  # noqa: S311
        string.ascii_letters + string.digits,
        k=settings.urls.id_length,
    ))
