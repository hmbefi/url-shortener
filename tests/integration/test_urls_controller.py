from fastapi import status
import pytest
from sqlalchemy import delete

from src.app.data_sources.models import UrlAlchemyModel


@pytest.mark.asyncio
async def test_get_full_url(test_client, session):
    response = await test_client.post(
        url='/short',
        content='{"url": "http://test.com"}',
    )
    assert response.status_code == status.HTTP_200_OK
    
    response = response.json()
    url_id = response.get('short_url').split('/')[-1]

    response = await test_client.get(
        url=f'/short/{url_id}',
        follow_redirects=False,
    )
    assert response.status_code == status.HTTP_301_MOVED_PERMANENTLY
    redirect = response.next_request
    assert redirect.url == 'http://test.com'

    await session.execute(
        delete(UrlAlchemyModel).where(
            UrlAlchemyModel.id == 'test',
        )
    )


@pytest.mark.asyncio
async def test_get_full_url_exception(test_client):
    response = await test_client.get(
        url='/short/12345',
        follow_redirects=False,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND