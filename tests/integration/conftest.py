from httpx import AsyncClient
import pytest_asyncio

from src.app.service import app
from src.app.data_sources.adaptor import get_session


@pytest_asyncio.fixture(scope='session')
async def test_client(session):

    async def test_session():
        return session
    app.dependency_overrides[get_session] = test_session

    async with AsyncClient(app=app, base_url='http://test') as test_client:
        yield test_client