from fastapi import status
import pytest


@pytest.mark.asyncio
async def test_up(test_client):
    response = await test_client.get('/healthz/up')
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_ready(test_client):
    response = await test_client.get('/healthz/ready')
    assert response.status_code == status.HTTP_200_OK
