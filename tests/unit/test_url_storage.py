import pytest
from sqlalchemy import insert, select, delete

from src.app.data_sources.models import UrlAlchemyModel
from src.app.data_sources.url_storage import UrlStorage

url_storage = UrlStorage()

@pytest.mark.asyncio
async def test_add_url(session):
    test_url = 'http://test.com'
    url_id = await url_storage.add_url(session=session, url=test_url)
    selected_row = (await session.execute(
        select(UrlAlchemyModel).where(
            UrlAlchemyModel.id == url_id,
        ),
    )).scalar()
    assert selected_row.url == test_url

    await session.execute(
        delete(UrlAlchemyModel).where(
            UrlAlchemyModel.id == url_id,
        )
    )


@pytest.mark.asyncio
async def test_get_url(session):
    await session.execute(
        insert(UrlAlchemyModel).values(
            id='test',
            url='test_url',
        ),
    )

    url_from_db = await url_storage.get_url(session=session, url_id='test')
    assert url_from_db == 'test_url'

    await session.execute(
        delete(UrlAlchemyModel).where(
            UrlAlchemyModel.id == 'test',
        )
    )
